const { Trade } = require('../model/trade.model')
const { Sequelize } = require("sequelize");
const defaultController = require('./default.controller');

/**
 * Get buy trades filtered by symbol
 * @param {string} symbol the symbol of trade
 * @returns Promise the queried trade list
 */
exports.getBuyTrades = async (symbol) => {
    return await getTradesByType(symbol, false);
}

/**
 * Get sell trades filtered by symbol
 * @param {string} symbol the symbol of trade
 * @returns Promise the queried trade list
 */
exports.getSellTrades = async (symbol) => {
    return await getTradesByType(symbol, true);
}

/**
 * Calcul benefits realized by all trades
 * @param {*} symbol the symbol to analyze
 * @returns Promise the benefits (loses if negative)
 */
exports.getBenefits = async (symbol) => {

    const buys = await this.getBuyTrades(symbol);
    const sells = await this.getSellTrades(symbol);
    let benefits = 0;
    let index = 0;

    sells.forEach(sell => {
        while (sell.amount > 0) {
            if (sell.amount > buys[index].amount) {
                // If sell amount is bigger than buy one we use the buy amount for the calcul and reduce sell amount
                benefits += buys[index].amount * sell.price - buys[index].amount * buys[index].price;
                // We reduce the sell amount for next loop
                sell.amount -= buys[index].amount;
                // We go to nex buy because we consumed this one by uses his maximum amount
                index++;
            } else {
                // If buy amount is bigger than sell one we use the sell amount for the calcul and reduce buy amount
                benefits += sell.amount * sell.price - sell.amount * buys[index].price;
                // We reduce buy amount for next sells calcul
                buys[index].amount -= sell.amount;
                // If we consumed the buy we go to next
                if (buys[index].amount < 10 ** -13) index++;
                // We just consumed the sell by uses his maximum amount
                sell.amount = 0;
            }
            if (sell.amount < 10 ** -13) sell.amount = 0;
        }
    });

    return +benefits.toFixed(2);
}

/**
 * Calcul benefits for all symbols
 * @returns Promise the benefices (loses if negative)
 */
exports.getAllBenefits = async () => {
    symbols = await this.getSymbols();

    let benefits = 0;

    for (let symbol of symbols) {
        temp = await this.getBenefits(symbol);
        benefits += temp;
    }

    return +benefits.toFixed(2);
}

/**
 * Get alls the symbols
 * @returns Promise the symbols
 */
exports.getSymbols = async () => {
    return (await Trade.findAll({
        attributes: [
            [Sequelize.fn('DISTINCT', Sequelize.col('symbol')), 'symbol'],
        ]
    })).map(symbol => symbol.symbol);
}

/**
 * Calcul average price for a symbol
 * @param {*} symbol the symbol to analyze
 * @returns Promise the average price for this symbol
 */
exports.getAveragePrice = async (symbol) => {
    const buys = await this.getBuyTrades(symbol);
    const sells = await this.getSellTrades(symbol);
    let index = 0;

    sells.forEach(sell => {
        while (sell.amount > 0) {
            if (sell.amount > buys[index].amount) {
                // We reduce the sell amount for next loop
                sell.amount -= buys[index].amount;
                // We go to nex buy because we consumed this one by uses his maximum amount
                index++;
            } else {
                // We reduce buy amount for next sells calcul
                buys[index].amount -= sell.amount;
                // If we consumed the buy we go to next
                if (buys[index].amount < 10 ** -13) index++;
                // We just consumed the sell by uses his maximum amount
                sell.amount = 0;
            }
            if (sell.amount < 10 ** -13) sell.amount = 0;
        }
    });

    let totalAmount = 0;
    let totalPrice = 0;

    for (; index < buys.length; index++) {
        totalAmount += buys[index].amount;
        totalPrice += buys[index].amount * buys[index].price
    }

    return +(totalPrice / totalAmount).toFixed(2);
}

/**
 * Get trades filtered by symbol and type
 * isSell false = buy trade
 * isSell true = sell trade
 * @param {string} symbol the symbol of trade
 * @param {boolean} isSell the sell marker
 * @returns Promise the queried trade list
 */
async function getTradesByType(symbol, isSell) {
    return await Trade.findAll({
        where: {
            symbol: symbol,
            sell: isSell
        },
        order: [
            ['timestamp', 'ASC'],
        ]
    })
}

/**
 * Get the actual amount for a symbol
 * @param {*} symbol the symbol to analyze
 * @returns Promise the actual amount for the symbol
 */
exports.getActualAmount = async (symbol) => {
    const buys = await this.getBuyTrades(symbol);
    const sells = await this.getSellTrades(symbol);
    let index = 0;

    sells.forEach(sell => {
        while (sell.amount > 0) {
            if (sell.amount > buys[index].amount) {
                // We reduce the sell amount for next loop
                sell.amount -= buys[index].amount;
                // We go to nex buy because we consumed this one by uses his maximum amount
                index++;
            } else {
                // We reduce buy amount for next sells calcul
                buys[index].amount -= sell.amount;
                // If we consumed the buy we go to next
                if (buys[index].amount < 10 ** -13) index++;
                // We just consumed the sell by uses his maximum amount
                sell.amount = 0;
            }
            if (sell.amount < 10 ** -13) sell.amount = 0;
        }
    });

    let actualAmount = 0;

    for (; index < buys.length; index++) {
        actualAmount += buys[index].amount;
    }

    return +actualAmount.toFixed(12);
}

/**
 * Get the actual money invested for a symbol
 * @param {*} symbol the symbol to analyze
 * @returns Promise the actual money invested for the symbol
 */
exports.getActualInvested = async (symbol) => {
    averagePrice = await this.getAveragePrice(symbol);
    actualAmount = await this.getActualAmount(symbol);
    return +(averagePrice * actualAmount).toFixed(12);
}

/**
 * Get the actual money invested for all symbols
 * @returns Promise the full money invested
 */
exports.getAllActualInvested = async () => {
    symbols = await this.getSymbols();

    let actualInvested = 0;

    for (let symbol of symbols) {
        temp = await this.getActualInvested(symbol);
        actualInvested += temp;
    }

    return +actualInvested.toFixed(12);
}

/**
 * Get benefits if sell all symbol at a defined price
 * @param {*} symbol the symbol to analyze
 * @param {*} price the sell price
 * @returns Promise gains for this full sell
 */
exports.simulateFullSell = async (symbol, price) => {
    const amountToSell = await this.getActualAmount(symbol);
    return await this.simulateSell(symbol, price, +amountToSell.toFixed(12));
}

/**
 * Get benefits of a potentially sell is done at a defined price
 * @param {*} symbol the symbol to analyze
 * @param {*} price the sell price
 * @param {*} amount the amount sell
 * @returns Promise gains for a sell
 */
exports.simulateSell = async (symbol, price, amount) => {
    const buys = await this.getBuyTrades(symbol);
    const sells = await this.getSellTrades(symbol);
    let index = 0;

    sells.forEach(sell => {
        while (sell.amount > 0) {
            if (sell.amount > buys[index].amount) {
                // We reduce the sell amount for next loop
                sell.amount -= buys[index].amount;
                // We go to nex buy because we consumed this one by uses his maximum amount
                index++;
            } else {
                // We reduce buy amount for next sells calcul
                buys[index].amount -= sell.amount;
                // If we consumed the buy we go to next
                if (buys[index].amount < 10 ** -13) index++;
                // We just consumed the sell by uses his maximum amount
                sell.amount = 0;
            }
        }
        if (sell.amount < 10 ** -13) sell.amount = 0;
    });

    // Now FIFO skipped, calcul benefit of this trade

    let benefits = 0;

    while (amount > 0) {
        if (amount > buys[index].amount) {
            // If sell amount is bigger than buy one we use the buy amount for the calcul and reduce sell amount
            benefits += buys[index].amount * price - buys[index].amount * buys[index].price;
            // We reduce the sell amount for next loop
            amount -= buys[index].amount;
            // We go to nex buy because we consumed this one by uses his maximum amount
            index++;
        } else {
            // If buy amount is bigger than sell one we use the sell amount for the calcul and reduce buy amount
            benefits += amount * price - amount * buys[index].price;
            // We reduce buy amount for next sells calcul
            buys[index].amount -= amount;
            // If we consumed the buy we go to next
            if (buys[index].amount < 10 ** -13) index++;
            // We just consumed the sell by uses his maximum amount
            amount = 0;
        }
        if (amount < 10 ** -13) amount = 0;
    }

    return +benefits.toFixed(12);
}

/**
 * Get full benefits for a symbol if we simulate a sell trade
 * @param {*} symbol the symbol to analyze
 * @param {*} price the sell price
 * @param {*} amount the simulated amount to sell
 * @returns Promise benefits after a sell simulation
 */
exports.simulateBenefits = async (symbol, price, amount) => {
    const benefits = await this.getBenefits(symbol);
    const simulatedBenefits = await this.simulateSell(symbol, price, amount);
    return +(benefits + simulatedBenefits).toFixed(2);
}

/**
 * Get full benefits for a symbol if we simulate a sull sell trade
 * @param {*} symbol the symbol to analyze
 * @param {*} price the sell price
 * @returns Promise benefits after a full sell simulation
 */
exports.simulateAllBenefits = async (symbol, price) => {
    const amount = await this.getActualAmount(symbol);
    return await this.simulateBenefits(symbol, price, amount);
}

/**
 * Get benefits of a potentially sell is done at a defined price
 * @param {*} symbol the symbol to analyze
 * @param {*} price the sell price
 * @param {*} percentage the percentage of amount to sell
 * @returns Promise gains after a partial sell simulation
 */
exports.simulatePartialSell = async (symbol, price, percentage) => {
    let amount = await this.getActualAmount(symbol);
    return await this.simulateSell(symbol, price, amount * percentage / 100);
}

/**
 * Determine the full benefits for a symbol after a partial sell simulation
 * @param {*} symbol the symbol to analyze
 * @param {*} price the simulated sell price
 * @param {*} percentage the percentage of amount sell
 * @returns Promise benefits after a partial sell simulation
 */
exports.simulatePartialBenefits = async (symbol, price, percentage) => {
    let amount = await this.getActualAmount(symbol);
    return await this.simulateBenefits(symbol, price, amount * percentage / 100);
}

/**
 * Get all actual amounts for all symbols
 * @returns Promise an array of an array of amounts 
 */
exports.getAllActualAmount = async () => {
    symbols = await this.getSymbols();

    let array = [];

    for (let symbol of symbols) {
        amount = await this.getActualAmount(symbol);
        array.push({
            symbol: symbol,
            amount: amount
        })
    }

    return array;
}

/**
 * Refresh the amount for a symbol by execute a trade with a price of 0
 * (A chain fee or a free obtain token is a sell or buy at 0)
 * @param {*} symbol the symbol to treat
 * @param {*} amount the real amount 
 */
exports.refreshAmount = async (symbol, amount) => {
    let theoricAmount = await this.getActualAmount(symbol);
    if (theoricAmount < amount) {
        let correction = amount - theoricAmount;
        if (correction < 10 ** -13) return;
        defaultController.create(Trade, {
            amount: correction.toFixed(12),
            price: 0,
            sell: false,
            symbol: symbol,
            timestamp: Date.now()
        })
    } else if (theoricAmount > amount) {
        let correction = theoricAmount - amount;
        if (correction < 10 ** -13) return;
        defaultController.create(Trade, {
            amount: correction.toFixed(12),
            price: 0,
            sell: true,
            symbol: symbol,
            timestamp: Date.now()
        })
    }
}